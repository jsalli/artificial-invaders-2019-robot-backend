from robot_controller import RobotController
from utils import parse_params_file


class RobotBackend():
    def __init__(self):
        self.game = RobotController()


    def get_observations(self, debug=False):
        return self.game.get_observations(debug)


def main():
    robot_backend = RobotBackend()

    while True:
        observations = robot_backend.get_observations(debug=True)
        print(observations)


if __name__ == "__main__":
    main()
