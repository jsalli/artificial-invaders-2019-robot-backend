import math
import json

from observation_utils import check_object_observation, check_walls_and_goals, generate_wall_and_goal_lines
from ip_cam_connection import IPCamConnection


NUMBER_OF_ACTIONS = 5
NUMBER_OF_AGENTS = 1

ANGLES = [-90, -45, -20, 0, 20, 45, 90]
RAY_LENGTH = 1.0
ROBOT_SPEED = 0.8
TURN_SPEED = 0.2

ARENA_PARAMS_PATH = "level_params/"


class RobotController():
    def __init__(self):
        print("RobotController init")
        self._angles = ANGLES
        self._ray_length = RAY_LENGTH
        self._turn_speed = TURN_SPEED
        self._robot_speed = ROBOT_SPEED
        self._ip_cam_connection = IPCamConnection()
        self._walls_and_goals_lines = generate_wall_and_goal_lines(ARENA_PARAMS_PATH)


    def get_observations(self, debug=False):
        (robot_coordinates,
         robot_rotation,
         green_coordinates,
         yellow_coordinates) = self._ip_cam_connection.get_ball_and_robot_coordinate(debug)

        if robot_coordinates is None or robot_rotation is None:
            print("Robot not found")
            return []
        local_angles = self._get_local_position_and_angles(robot_rotation, self._angles[:])
        object_coordinates = [green_coordinates, yellow_coordinates]# + [[[]]] # The empty array is enemy robot location
        ball_obs = check_object_observation(robot_coordinates, local_angles, self._ray_length, object_coordinates)
        wall_obs = check_walls_and_goals(robot_coordinates, local_angles, self._ray_length, self._walls_and_goals_lines)
        return [*ball_obs, *wall_obs]


    def _get_local_position_and_angles(self, robot_rotation, angles):
        local_rotation = robot_rotation -90
        local_angles = angles
        for i in range(len(local_angles)):
            local_angles[i] = local_angles[i] - local_rotation + 90

        return local_angles


    # For debuging
    def get_ball_observation(self, debug=False):
        (robot_coordinates,
         robot_rotation,
         green_coordinates,
         yellow_coordinates) = self._ip_cam_connection.get_ball_and_robot_coordinate(debug)

        if robot_coordinates is None or robot_rotation is None:
            print("Robot not found")
            return []
        local_angles = self._get_local_position_and_angles(robot_rotation, self._angles[:])
        object_coordinates = [green_coordinates, yellow_coordinates]# + [[[]]] + [[[]]] # The empty arrays are green balls and enemy robot location
        return check_object_observation(robot_coordinates, local_angles, self._ray_length, object_coordinates)


    # For debuging
    def get_other_observation(self, debug=False):
        (robot_coordinates,
         robot_rotation,
         green_coordinates,
         yellow_coordinates) = self._ip_cam_connection.get_ball_and_robot_coordinate(debug)

        if robot_coordinates is None or robot_rotation is None:
            print("Robot not found")
            return []
        local_angles = self._get_local_position_and_angles(robot_rotation, self._angles[:])
        return check_walls_and_goals(robot_coordinates, local_angles, self._ray_length, self._walls_and_goals_lines)


    def do_action(self, action):
        self._move(action)
        return 0


    def number_of_observations(self):
        return len(self.get_observations())


    def get_number_of_actions(self):
        return NUMBER_OF_ACTIONS


    def _set_motors(self, l_motor, r_motor):
        if l_motor is not None and r_motor is not None:
            # TODO: GRPC to actual robot
            pass


    def _move(self, action):
        l_motor = None
        r_motor = None

        # Forward
        if action == 1:
            l_motor = self._robot_speed
            r_motor = self._robot_speed
        # Backward
        elif action == 2:
            l_motor = -self._robot_speed
            r_motor = -self._robot_speed
        # Turn Clockwise
        elif action == 3:
            l_motor = self._robot_speed * self._turn_speed
            r_motor = -self._robot_speed * self._turn_speed
        # Turn Anti Clockwise
        elif action == 4:
            l_motor = -self._robot_speed * self._turn_speed
            r_motor = self._robot_speed * self._turn_speed
        # No action
        elif action == 0:
            pass
        else:
            raise Exception("Unknown action {}".format(action))

        self._set_motors(l_motor, r_motor)


# For testing
def main():
    robot_controller = RobotController()
    try:
        while True:
            # ball_obs = robot_controller.get_ball_observation(debug=True)
            # ball_obs[::4] = ['%.0f' % elem for elem in ball_obs[::4]]
            # ball_obs[1::4] = ['%.0f' % elem for elem in ball_obs[1::4]]
            # ball_obs[2::4] = ['%.0f' % elem for elem in ball_obs[2::4]]
            # ball_obs[3::4] = ['%.2f' % elem for elem in ball_obs[3::4]]
            # print("===")
            # print("==== Sector 0: {} | 90-45 right".format(ball_obs[0:4]))
            # print("==== Sector 1: {} | 45-20 right".format(ball_obs[4:8]))
            # print("==== Sector 2: {} | 20-0 right".format(ball_obs[8:12]))
            # print("==== Sector 3: {} | 20-0 left".format(ball_obs[12:16]))
            # print("==== Sector 4: {} | 45-20 left".format(ball_obs[16:20]))
            # print("==== Sector 5: {} | 90-45 left".format(ball_obs[20:24]))
            # print("===")

            other_obs = robot_controller.get_other_observation(debug=True)
            other_obs[::5] = ['%.0f' % elem for elem in other_obs[::5]]
            other_obs[1::5] = ['%.0f' % elem for elem in other_obs[1::5]]
            other_obs[2::5] = ['%.0f' % elem for elem in other_obs[2::5]]
            other_obs[3::5] = ['%.0f' % elem for elem in other_obs[3::5]]
            other_obs[4::5] = ['%.2f' % elem for elem in other_obs[4::5]]
            print("===")
            print("==== Sector 0: {} | 90 right".format(other_obs[0:5]))
            print("==== Sector 1: {} | 45 right".format(other_obs[5:10]))
            print("==== Sector 2: {} | 20 right".format(other_obs[10:15]))
            print("==== Sector 3: {} | forward".format(other_obs[15:20]))
            print("==== Sector 4: {} | 20 left".format(other_obs[20:25]))
            print("==== Sector 5: {} | 45 left".format(other_obs[25:30]))
            print("==== Sector 6: {} | 90 left".format(other_obs[30:35]))

    except KeyboardInterrupt:
        print("Closing")


if __name__ == "__main__":
    main()
