import numpy as np
import math
import cv2
from cv2 import aruco
import json


def parse_params_file(params_file_path):
    with open(params_file_path, 'r') as file:
        json_string = file.read()
        parameters = json.loads(json_string)

    return parameters


def _is_rotation_matrix(R):
    '''
    Checks if a matrix is a valid rotation matrix.
    https://www.learnopencv.com/rotation-matrix-to-euler-angles/
    '''
    r_t = np.transpose(R)
    should_be_identity = np.dot(r_t, R)
    I = np.identity(3, dtype=R.dtype)
    n = np.linalg.norm(I - should_be_identity)
    return n < 1e-6


def _rotation_matrix_to_euler_angles(R):
    '''
    Calculates rotation matrix to euler angles
    The result is the same as MATLAB except the order
    of the euler angles ( x and z are swapped ).
    https://www.learnopencv.com/rotation-matrix-to-euler-angles/
    '''
    assert _is_rotation_matrix(R)
    sy = math.sqrt(R[0, 0] * R[0, 0] +  R[1, 0] * R[1, 0])
    singular = sy < 1e-6
    if not singular:
        x = math.atan2(R[2, 1], R[2, 2])
        y = math.atan2(-R[2, 0], sy)
        z = math.atan2(R[1, 0], R[0, 0])
    else:
        x = math.atan2(-R[1, 2], R[1, 1])
        y = math.atan2(-R[2, 0], sy)
        z = 0

    return np.array([math.degrees(x), math.degrees(y), math.degrees(z)])


def detect_aruco_marker(image, aruco_dict, parameters, camera_calib_params, size_of_marker, debug=False, length_of_axis=0.05):
    '''
    returns
    center: Array where element 0 is x-coordinate and element 1 is y-coordinate
    euler_angles: z, y, x coordinates in degrees
    '''
    mtx = np.array(camera_calib_params["mtx"])
    dist = np.array(camera_calib_params["dist"])
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    corners, ids, rejected_img_points = aruco.detectMarkers(gray, aruco_dict, parameters=parameters)

    center = None
    euler_angles = None
    if corners:
        center = np.mean(corners[0][0], axis=0)
        rvecs, tvecs, _ = aruco.estimatePoseSingleMarkers(corners, size_of_marker, mtx, dist)

        if rvecs is not None:
            dst, jacobian = cv2.Rodrigues(rvecs[0][0])
            euler_angles = _rotation_matrix_to_euler_angles(dst)

        if debug and tvecs is not None and rvecs is not None:
            debug_image = image.copy()
            imaxis = aruco.drawDetectedMarkers(debug_image, corners, ids)
            for i in range(len(tvecs)):
                imaxis = aruco.drawAxis(imaxis, mtx, dist, rvecs[i], tvecs[i], length_of_axis)
            
            aruco.drawDetectedMarkers(debug_image, corners, ids)
            aruco.drawDetectedMarkers(debug_image, rejected_img_points, borderColor=(100, 0, 240))
            cv2.imshow('Detect Acuro Marker', debug_image)

    return center, euler_angles
