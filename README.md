## Setup a virtual environment
This repo is tested with Python 3.6.8.

Create Python virtual environment if you want.
```
virtualenv venv
```
```
source venv/bin/activate
```

### Install Python packages
```
pip install -r requirements.txt
```


### Camera calibration
Original code is from [here](https://mecaruco.readthedocs.io/en/latest/notebooks_rst/aruco_calibration.html)