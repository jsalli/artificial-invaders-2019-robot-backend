# import numpy as np
import cv2
import cv2.aruco as aruco


'''
    drawMarker(...)
        drawMarker(dictionary, id, sidePixels[, img[, borderBits]]) -> img
'''

ARUCO_CODE = 2
AURCO_SIZE = 700

aruco_dict = aruco.Dictionary_get(aruco.DICT_6X6_250)
print(aruco_dict)
# second parameter is id number
# last parameter is total image size
img = aruco.drawMarker(aruco_dict, ARUCO_CODE, AURCO_SIZE)
cv2.imwrite("aruco_marker_code={}_size={}.jpg".format(ARUCO_CODE, AURCO_SIZE), img)

# Show the created marker
# cv2.imshow('frame', img)
# cv2.waitKey(0)
# cv2.destroyAllWindows()
