import cv2
from cv2 import aruco
import numpy as np
import imutils
import json
import math
from utils import detect_aruco_marker


VIDEO_URL = "http://192.168.10.55:8080/videofeed"
# Original size 640x480
CROPPED_VIDEO_HEIGHT = 480
CROPPED_VIDEO_WIDTH = 480
CROPPED_SIZE = 480

# H: 0-179, S: 0-255, V: 0-255
LOW_GREEN_HSV = [40, 60, 60]
HIGH_GREEN_HSV = [95, 255, 255]
LOW_YELLOW_HSV = [15, 80, 124]
HIGH_YELLOW_HSV = [42, 255, 255]

LOW_GREEN = np.array(LOW_GREEN_HSV)
HIGH_GREEN = np.array(HIGH_GREEN_HSV)
LOW_YELLOW = np.array(LOW_YELLOW_HSV)
HIGH_YELLOW = np.array(HIGH_YELLOW_HSV)

MIN_BALL_AREA = 150

SIZE_OF_MARKER = 0.047
LENGTH_OF_AXIS = 0.05


class IPCamConnection():
    def __init__(self):
        self._video_capture = cv2.VideoCapture(VIDEO_URL)
        self._aruco_dict = aruco.Dictionary_get(aruco.DICT_6X6_250)
        self._parameters = aruco.DetectorParameters_create()
        self._camera_calib_params = None
        with open('calib-params.json') as json_file:
            self._camera_calib_params = json.load(json_file)
        
        self._vectorized_scale_coordinate = np.vectorize(self._scale_coordinate)


    def close(self):
        cv2.destroyAllWindows()
        self._video_capture.release()


    @staticmethod
    def _crop_image(image):
        # startY and endY coordinates, startX and endX coordinates
        cropped = image[0:CROPPED_VIDEO_HEIGHT, 80:CROPPED_VIDEO_HEIGHT+80]
        return cropped


    @staticmethod
    def _find_balls_by_color(hsv_image, orig_image, low_color, high_color):
        color_mask = cv2.inRange(hsv_image, low_color, high_color)
        color_mask = cv2.erode(color_mask, None, iterations=2)
        color_mask = cv2.dilate(color_mask, None, iterations=2)
        color_image = cv2.bitwise_and(orig_image, orig_image, mask=color_mask)
        return color_image, color_mask


    @staticmethod
    def _blur_and_hsv(image):
        blurred_frame = cv2.GaussianBlur(image, (5, 5), 0)
        hsv_image = cv2.cvtColor(blurred_frame, cv2.COLOR_BGR2HSV)
        return hsv_image


    @staticmethod
    def _find_center_points(color_mask, orig_image=None):
        center_points = []
        contours = cv2.findContours(color_mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        contours = imutils.grab_contours(contours)

        for contour in contours:
            if cv2.contourArea(contour) < MIN_BALL_AREA:
                continue

            if orig_image is not None:
                cv2.drawContours(orig_image, contour, -1, (0, 255, 0), 3)
            # compute the center of the contour
            moments = cv2.moments(contour)
            center_x = int(moments["m10"] / moments["m00"])
            center_y = int(moments["m01"] / moments["m00"])
            center_points.append([center_x, center_y])
        return np.array(center_points)


    # @staticmethod
    # def _scale_coordinates(x_coordinate, y_coordinate):
    #     x_scaled = (x_coordinate - CROPPED_VIDEO_WIDTH / 2) / (CROPPED_VIDEO_WIDTH / 4)
    #     y_scaled = (y_coordinate - CROPPED_VIDEO_HEIGHT / 2) / (CROPPED_VIDEO_HEIGHT / 4)
    #     return x_scaled, y_scaled


    @staticmethod
    def _scale_coordinate(coordinate):
        scaled = (coordinate - CROPPED_SIZE / 2) / (CROPPED_SIZE / 4)
        return scaled


    # @staticmethod
    def _scale_coordinates_array(self, coordinates_array):
        if coordinates_array is not None and coordinates_array.any():
            return self._vectorized_scale_coordinate(coordinates_array)
        return coordinates_array

    # def _scale_coordinates_array(self, coordinates_array):
    #     scaled = np.arra
        # if coordinates_array is None or not coordinates_array.any():
        #     return None

        # if not isinstance(coordinates_array[0], np.ndarray):
        #     scaled.extend([*self._scale_coordinates(coordinates_array[0], coordinates_array[1])])
        # else:
        #     for inner_array in coordinates_array:
        #         inner_list = []
        #         inner_list.extend([*self._scale_coordinates(inner_array[0], inner_array[1])])
        #         scaled.append(inner_list)

        # return scaled


    def _get_image(self):
        success, image_frame = self._video_capture.read()
        if success is True:
            return self._crop_image(image_frame)
        
        print("_get_image is none")
        return None


    def get_ball_coordinates(self, image, debug=False):
        if image is None:
            image = self._get_image()
        hsv_image = self._blur_and_hsv(image)

        green_balls_image, green_mask = self._find_balls_by_color(hsv_image, image, LOW_GREEN, HIGH_GREEN)
        yellow_balls_image, yellow_mask = self._find_balls_by_color(hsv_image, image, LOW_YELLOW, HIGH_YELLOW)

        green_coordinates = self._find_center_points(green_mask)
        if debug:
            for coordinate in green_coordinates:
                cv2.circle(image, (coordinate[0], coordinate[1]), 5, (0, 0, 255), -1)

        yellow_coordinates = self._find_center_points(yellow_mask)
        if debug:
            for coordinate in yellow_coordinates:
                cv2.circle(image, (coordinate[0], coordinate[1]), 5, (0, 0, 255), -1)

        if debug:
            cv2.imshow('Ball Coordinates', image)

        return self._scale_coordinates_array(green_coordinates), self._scale_coordinates_array(yellow_coordinates)


    def get_robot_coordinates(self, image, debug=False):
        if image is None:
            image = self._get_image()
        robot_coordinates, robot_rotation = detect_aruco_marker(image, self._aruco_dict, self._parameters, self._camera_calib_params, SIZE_OF_MARKER, debug)
        x_rotation = robot_rotation[2] if robot_rotation is not None else None
        return self._scale_coordinates_array(robot_coordinates), x_rotation


    def get_ball_and_robot_coordinate(self, debug=False):
        image = self._get_image()
        if image is None:
            return None, None, None, None
        robot_coordinates, robot_rotation = self.get_robot_coordinates(image, debug)
        green_coordinates, yellow_coordinates = self.get_ball_coordinates(image, debug)
        if debug:
            # cv2.waitKey is needed to show the image when calling 'cv2.imshow()'
            cv2.waitKey(1)

        return robot_coordinates, robot_rotation, green_coordinates, yellow_coordinates


# For testing
def main():
    '''
    Connect to IP cam and print results
    '''
    ip_cam_connection = IPCamConnection()
    try:
        while True:
            (robot_coordinates,
             robot_rotation,
             green_coordinates,
             yellow_coordinates) = ip_cam_connection.get_ball_and_robot_coordinate(debug=True)

            print("=== robot_coordinates")
            print(robot_coordinates)
            print("=== robot_rotation")
            print(robot_rotation)
            print("=== green_coordinates")
            print(green_coordinates)
            print("=== yellow_coordinates")
            print(yellow_coordinates)

            if cv2.waitKey(1) & 0xFF == ord('q'):
                break

    except KeyboardInterrupt:
        print("Closing")
    # except Exception as e:
    #     print("Error")
    #     print(e)
    finally:
        ip_cam_connection.close()


if __name__ == "__main__":
    main()
